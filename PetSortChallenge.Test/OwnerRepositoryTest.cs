using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PetSortChallenge.Data.DAL;
using PetSortChallenge.Data.Mocks.MockDAL;
using PetSortChallenge.Data.Repositories;

namespace PetSortChallenge.Test
{
    [TestClass]
    public class OwnerRepositoryTest
    {
        [TestMethod]
        public void TestGetPetsFemale()
        {
            IOwnerDAL mockOwnerDAL = new MockOwnerDAL();
            IOwnerRepository ownerRepository = new OwnerRepository(mockOwnerDAL);

            var result = ownerRepository.GetPets(Data.Models.PetType.Cat, Data.Models.Gender.Female);

            Assert.AreEqual(result.Count(), 3);
            Assert.AreEqual(result.FirstOrDefault().PetName, "Garfield");
            Assert.AreEqual(result.LastOrDefault().PetName, "Tabby");
        }

        [TestMethod]
        public void TestGetPetsMale()
        {
            IOwnerDAL mockOwnerDAL = new MockOwnerDAL();
            IOwnerRepository ownerRepository = new OwnerRepository(mockOwnerDAL);

            var result = ownerRepository.GetPets(Data.Models.PetType.Cat, Data.Models.Gender.Male);

            Assert.AreEqual(result.Count(), 4);
            Assert.AreEqual(result.FirstOrDefault().PetName, "Garfield");
            Assert.AreEqual(result.LastOrDefault().PetName, "Tom");
        }
    }
}
