﻿using System;
using System.Collections.Generic;
using System.Linq;
using PetSortChallenge.Data.DAL;
using PetSortChallenge.Data.Mocks.MockDAL;
using PetSortChallenge.Data.Models;
using PetSortChallenge.Data.Repositories;

namespace PetSortChallenge.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            IOwnerDAL ownerDAL = new OwnerDAL();
            IOwnerRepository ownerRepository = new OwnerRepository(ownerDAL);

            // Print list of pets for Female owners
            var fresults = ownerRepository.GetPets(PetType.Cat, Gender.Female);
            System.Console.WriteLine(Gender.Female);
            foreach (var pet in fresults)
            {
                pet.Print();
            }

            System.Console.WriteLine();

            // Print list of pets for Male owners
            var mresults = ownerRepository.GetPets(PetType.Cat, Gender.Male); 
            System.Console.WriteLine(Gender.Male);
            foreach (var pet in mresults)
            {
                pet.Print();
            }
        }
    }
}
