﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PetSortChallenge.Data.DAL
{
    public class OwnerDAL : IOwnerDAL
    {
        private const string URL = "http://agl-developer-test.azurewebsites.net/people.json";
        private string urlParameters = "";

        public OwnerDAL()
        {
        }

        /// <summary>
        /// Gets the owners.
        /// </summary>
        /// <returns>The owners.</returns>
        public string GetOwners()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                return result;
            }
            else
            {
                return string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }  
        }
    }
}
