﻿using System;
namespace PetSortChallenge.Data.DAL
{
    public interface IOwnerDAL
    {
        string GetOwners();
    }
}
