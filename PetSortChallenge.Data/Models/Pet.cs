﻿using System;
using Newtonsoft.Json;

namespace PetSortChallenge.Data.Models
{
    public class Pet
    {
        public Pet()
        {
        }

        public Pet(string petname, PetType pettype )
        {
            PetName = petname;
            PetType = pettype;
        }

        [JsonProperty("name")]
        public string PetName { get; set; }

        [JsonProperty("type")]
        public PetType PetType { get; set; }
    }
}
