﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PetSortChallenge.Data.Models
{
    public class Owner
    {
        public Owner()
        {
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("age")]
        public string Age { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("pets")]
        public List<Pet> Pets { get; set; }
    }
}
