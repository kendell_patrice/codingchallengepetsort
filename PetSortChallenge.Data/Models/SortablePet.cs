﻿using System;
namespace PetSortChallenge.Data.Models
{
    public class SortablePet : Pet
    {
        public SortablePet()
        {
        }

        public string OwnerGender { get; set; }

        public void Print()
        {
            System.Console.WriteLine(PetName);
        }
    }
}
