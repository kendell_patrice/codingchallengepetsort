﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PetSortChallenge.Data.Models
{
    public class PetCollection
    {
        IEnumerable<Owner> _owners = null;

        public PetCollection(IEnumerable<Owner> owners)
        {
            _owners = owners;
        }

        /// <summary>
        /// Gets the pet collection.
        /// </summary>
        /// <returns>The pet collection.</returns>
        /// <param name="gender">Gender.</param>
        /// <param name="petType">Pet type.</param>
        public IEnumerable<SortablePet> GetPetCollection(Gender gender, PetType petType)
        {
            var sortablePets = GroupPets(petType);
            var pets = Sort(FilterByGender(sortablePets, gender));

            return pets;
        }

        /// <summary>
        /// Groups the pets.
        /// </summary>
        /// <returns>The pets.</returns>
        /// <param name="petType">Pet type.</param>
        private IEnumerable<SortablePet> GroupPets(PetType petType)
        {
            var withpets = from i in _owners where i.Pets != null select i;
            var result = from i in withpets
                         from e in i.Pets
                         where i.Pets != null && e.PetType == petType
                         select new SortablePet()
                         {
                             PetName = e.PetName,
                             PetType = e.PetType,
                             OwnerGender = i.Gender
                         };

            return result;
        }

        /// <summary>
        /// Filters the by gender.
        /// </summary>
        /// <returns>The by gender.</returns>
        /// <param name="sortablePets">Sortable pets.</param>
        /// <param name="gender">Gender.</param>
        private IEnumerable<SortablePet> FilterByGender(IEnumerable<SortablePet> sortablePets, Gender gender)
        {
            var result = from i in sortablePets
                         where i.OwnerGender.Equals(gender.ToString(), StringComparison.InvariantCultureIgnoreCase)
                         select i;

            return result;
        }

        /// <summary>
        /// Sort the specified pets.
        /// </summary>
        /// <returns>The sort.</returns>
        /// <param name="pets">Pets.</param>
        private IEnumerable<SortablePet> Sort(IEnumerable<SortablePet> pets)
        {
            var result = from i in pets
                         orderby i.PetName ascending
                         select i;

            return result;
        }
    }
}
