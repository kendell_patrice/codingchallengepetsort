﻿using System;
namespace PetSortChallenge.Data.Models
{
    public enum PetType
    {
        Cat,
        Dog,
        Fish
    }

    public enum Gender
    {
        Male, Female
    }
}
