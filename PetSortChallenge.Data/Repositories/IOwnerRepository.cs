﻿using System;
using System.Collections.Generic;
using PetSortChallenge.Data.Models;

namespace PetSortChallenge.Data.Repositories
{
    public interface IOwnerRepository
    {
        IEnumerable<SortablePet> GetPets(PetType petType, Gender gender);
    }
}
