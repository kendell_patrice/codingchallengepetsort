﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using PetSortChallenge.Data.DAL;
using PetSortChallenge.Data.Models;

namespace PetSortChallenge.Data.Repositories
{
    public class OwnerRepository : IOwnerRepository
    {
        IOwnerDAL _ownerdal = null;

        public OwnerRepository(IOwnerDAL ownerdal)
        {
            _ownerdal = ownerdal;
        }

        /// <summary>
        /// Gets the pets.
        /// </summary>
        /// <returns>The pets.</returns>
        /// <param name="petType">Pet type.</param>
        /// <param name="gender">Gender.</param>
        public IEnumerable<SortablePet> GetPets(PetType petType, Gender gender)
        {
            var result = _ownerdal.GetOwners();
            var owners = JsonConvert.DeserializeObject<List<Owner>>(result);
            var petCollection = new PetCollection(owners);

            return petCollection.GetPetCollection(gender, petType);
        }
    }
}
