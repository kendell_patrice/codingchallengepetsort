﻿using System;
using System.Collections.Generic;
using PetSortChallenge.Data.DAL;
using PetSortChallenge.Data.Models;
using PetSortChallenge.Data.Repositories;

namespace PetSortChallenge.Data.Mocks.MockRepositories
{
    public class MockOwnerRepository : IOwnerRepository
    {
        IOwnerDAL _ownerdal = null;

        public MockOwnerRepository(IOwnerDAL ownerdal)
        {
            _ownerdal = ownerdal;
        }

        public IEnumerable<SortablePet> GetPets(PetType petType, Gender gender)
        {
            throw new NotImplementedException();
        }
    }
}
